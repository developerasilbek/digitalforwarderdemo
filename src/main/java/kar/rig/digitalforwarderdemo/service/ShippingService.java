package kar.rig.digitalforwarderdemo.service;

import kar.rig.digitalforwarderdemo.domain.Cargo;
import kar.rig.digitalforwarderdemo.domain.Shipping;
import kar.rig.digitalforwarderdemo.domain.enumeration.ShippingStatus;
import kar.rig.digitalforwarderdemo.repository.CargoRepository;
import kar.rig.digitalforwarderdemo.repository.ShippingRepository;
import kar.rig.digitalforwarderdemo.service.dto.CargoDTO;
import kar.rig.digitalforwarderdemo.service.dto.ShippingDTO;
import kar.rig.digitalforwarderdemo.service.mapper.CargoMapper;
import kar.rig.digitalforwarderdemo.service.mapper.ShippingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ShippingService {

    private final Logger log = LoggerFactory.getLogger(ShippingService.class);

    private final ShippingRepository shippingRepository;

    private final CargoRepository cargoRepository;

    private final ShippingMapper shippingMapper;

    private final CargoMapper cargoMapper;

    public ShippingService(ShippingRepository shippingRepository, CargoRepository cargoRepository, ShippingMapper shippingMapper, CargoMapper cargoMapper) {
        this.shippingRepository = shippingRepository;
        this.cargoRepository = cargoRepository;
        this.shippingMapper = shippingMapper;
        this.cargoMapper = cargoMapper;
        log.info("Start ->>>>>>>");
    }

    public ShippingDTO save(ShippingDTO shippingDTO, CargoDTO cargoDTO) {
        shippingDTO.setStatus(ShippingStatus.NEW);
        log.debug("Request to save Shipping : {}", shippingDTO);
        Cargo cargo = cargoMapper.toEntity(cargoDTO);
        cargo = cargoRepository.save(cargo);
        Shipping shipping = shippingMapper.toEntity(shippingDTO);
        shipping = shippingRepository.save(shipping);
        shipping.setCargo(cargo);
        return shippingMapper.toDto(shipping);
    }

    public ShippingDTO update(ShippingDTO shippingDTO) {
        log.debug("Request to update Shipping : {}", shippingDTO);
        Shipping address = shippingMapper.toEntity(shippingDTO);
        address = shippingRepository.save(address);
        return shippingMapper.toDto(address);
    }

    public Optional<ShippingDTO> partialUpdate(ShippingDTO shippingDTO) {
        log.debug("Request to partially update Shipping : {}", shippingDTO);

        return shippingRepository
                .findById(shippingDTO.getId())
                .map(existingAddress -> {
                    shippingMapper.partialUpdate(existingAddress, shippingDTO);

                    return existingAddress;
                })
                .map(shippingRepository::save)
                .map(shippingMapper::toDto);
    }

    public Optional<ShippingDTO> findOne(Long id) {
        log.debug("Request to get Shipping : {}", id);
        return shippingRepository.findById(id).map(shippingMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete Shipping : {}", id);
        shippingRepository.deleteById(id);
    }

    public List<ShippingDTO> findAll() {
        return shippingRepository
                .findAll()
                .stream()
                .map(shippingMapper::toDto)
                .collect(Collectors.toList());
    }
}
