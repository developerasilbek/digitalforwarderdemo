package kar.rig.digitalforwarderdemo.service;

import kar.rig.digitalforwarderdemo.domain.User;
import kar.rig.digitalforwarderdemo.repository.UserRepository;
import kar.rig.digitalforwarderdemo.service.dto.UserDTO;
import kar.rig.digitalforwarderdemo.service.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        log.info("Start ->>>>>>>");
    }

    public UserDTO save(UserDTO userDTO) {
        log.debug("Request to save User : {}", userDTO);
        User user = userMapper.toEntity(userDTO);
        user = userRepository.save(user);
        return userMapper.toDto(user);
    }

    public UserDTO update(UserDTO userDTO) {
        log.debug("Request to update User : {}", userDTO);
        User address = userMapper.toEntity(userDTO);
        address = userRepository.save(address);
        return userMapper.toDto(address);
    }

    public Optional<UserDTO> partialUpdate(UserDTO userDTO) {
        log.debug("Request to partially update User : {}", userDTO);

        return userRepository
                .findById(userDTO.getId())
                .map(existingAddress -> {
                    userMapper.partialUpdate(existingAddress, userDTO);

                    return existingAddress;
                })
                .map(userRepository::save)
                .map(userMapper::toDto);
    }

    public Optional<UserDTO> findByMail(String email) {
        log.debug("Request to get User by email : {}", email);
        return userRepository.findUserByEmail(email).map(userMapper::toDto);
    }

    public Optional<UserDTO> findOne(Long id) {
        log.debug("Request to get User : {}", id);
        return userRepository.findById(id).map(userMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete User : {}", id);
        userRepository.deleteById(id);
    }

    public List<UserDTO> findAll() {
        return userRepository
                .findAll()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }
}
