package kar.rig.digitalforwarderdemo.service.mapper;

import kar.rig.digitalforwarderdemo.domain.Shipping;
import kar.rig.digitalforwarderdemo.service.dto.ShippingDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ShippingMapper extends EntityMapper<ShippingDTO, Shipping> {}
