package kar.rig.digitalforwarderdemo.service.mapper;

import kar.rig.digitalforwarderdemo.domain.User;
import kar.rig.digitalforwarderdemo.service.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDTO, User> {
}
