package kar.rig.digitalforwarderdemo.service.mapper;

import kar.rig.digitalforwarderdemo.domain.Cargo;
import kar.rig.digitalforwarderdemo.service.dto.CargoDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CargoMapper extends EntityMapper<CargoDTO, Cargo> {
}
