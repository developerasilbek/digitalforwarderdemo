package kar.rig.digitalforwarderdemo.service.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import kar.rig.digitalforwarderdemo.domain.Cargo;
import kar.rig.digitalforwarderdemo.domain.enumeration.CarcaseType;
import kar.rig.digitalforwarderdemo.domain.enumeration.CurrencyType;
import kar.rig.digitalforwarderdemo.domain.enumeration.ShippingStatus;
import kar.rig.digitalforwarderdemo.domain.enumeration.TruckModel;

public class ShippingDTO implements Comparable<ShippingDTO> {

    private Long id;

    @NotNull
    @NotEmpty
    private String senderCountry;

    @NotNull
    @NotEmpty
    private String senderAddress;

    @NotNull
    @NotEmpty
    private String recipientCountry;

    @NotNull
    @NotEmpty
    private String recipientAddress;

    private TruckModel truckModel;

    private CarcaseType carcaseType;

    @NotNull
    private Boolean insurance;

    @Min(value = 0, message = "Cost should not be less than 0")
    private Double totalCost;

    private CurrencyType currency;

    private ShippingStatus status;

    private Integer travelTime;

    private CargoDTO cargoDTO;

    public CargoDTO getCargoDTO() {
        return cargoDTO;
    }

    public void setCargoDTO(CargoDTO cargoDTO) {
        this.cargoDTO = cargoDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderCountry() {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry) {
        this.senderCountry = senderCountry;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getRecipientCountry() {
        return recipientCountry;
    }

    public void setRecipientCountry(String recipientCountry) {
        this.recipientCountry = recipientCountry;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public TruckModel getTruckModel() {
        return truckModel;
    }

    public void setTruckModel(TruckModel truckModel) {
        this.truckModel = truckModel;
    }

    public CarcaseType getCarcaseType() {
        return carcaseType;
    }

    public void setCarcaseType(CarcaseType carcaseType) {
        this.carcaseType = carcaseType;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public ShippingStatus getStatus() {
        return status;
    }

    public void setStatus(ShippingStatus status) {
        this.status = status;
    }

    public Integer getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(Integer travelTime) {
        this.travelTime = travelTime;
    }

    @Override
    public int compareTo(ShippingDTO shippingDTO) {
        return getId().compareTo(shippingDTO.getId());
    }


    @Override
    public String toString() {
        return "ShippingDTO{" +
                "id=" + id +
                ", senderCountry='" + senderCountry + '\'' +
                ", senderAddress='" + senderAddress + '\'' +
                ", recipientCountry='" + recipientCountry + '\'' +
                ", recipientAddress='" + recipientAddress + '\'' +
                ", truckModel=" + truckModel +
                ", carcaseType=" + carcaseType +
                ", insurance=" + insurance +
                ", totalCost=" + totalCost +
                ", currency=" + currency +
                ", status=" + status +
                ", travelTime=" + travelTime +
                ", cargoDTO=" + cargoDTO +
                '}';
    }
}
