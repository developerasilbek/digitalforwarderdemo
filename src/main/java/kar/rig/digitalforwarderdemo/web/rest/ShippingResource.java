package kar.rig.digitalforwarderdemo.web.rest;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import kar.rig.digitalforwarderdemo.domain.enumeration.CurrencyType;
import kar.rig.digitalforwarderdemo.domain.enumeration.ShippingStatus;
import kar.rig.digitalforwarderdemo.repository.ShippingRepository;
import kar.rig.digitalforwarderdemo.service.ShippingService;
import kar.rig.digitalforwarderdemo.service.dto.CargoDTO;
import kar.rig.digitalforwarderdemo.service.dto.ShippingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api")
public class ShippingResource {

    private final Logger log = LoggerFactory.getLogger(ShippingResource.class);

    private final ShippingService shippingService;

    public ShippingResource(ShippingService shippingService) {
        this.shippingService = shippingService;
    }

    @PostMapping("/shipping")
    public String createShipping(@Valid @ModelAttribute ShippingDTO shippingDTO, @ModelAttribute CargoDTO cargoDTO) throws Exception {
        log.debug("REST request to save Shipping : {}", shippingDTO);
        if (shippingDTO.getId() != null) {
            throw new Exception("A new shipping cannot already have an ID");
        }
        ShippingDTO result = shippingService.save(shippingDTO, cargoDTO);
        return "addShipping";
    }

//    @PutMapping("/shipping/{id}")
//    public ResponseEntity<ShippingDTO> updateShipping(
//            @PathVariable(value = "id", required = false) final Long id,
//            @Valid @RequestBody ShippingDTO shippingDTO
//    ) throws Exception {
//        log.debug("REST request to update Shipping : {}, {}", id, shippingDTO);
//        if (shippingDTO.getId() == null) {
//            throw new Exception("Invalid id");
//        }
//        if (!Objects.equals(id, shippingDTO.getId())) {
//            throw new Exception("Invalid ID");
//        }
//
//        if (!shippingRepository.existsById(id)) {
//            throw new Exception("Entity not found");
//        }
//
//        ShippingDTO result = shippingService.update(shippingDTO);
//        return ResponseEntity
//                .ok()
//                .headers(HttpHeaders.EMPTY)
//                .body(result);
//    }

//    @PatchMapping(value = "/shipping/{id}", consumes = {"application/json", "application/merge-patch+json"})
//    public ResponseEntity<ShippingDTO> partialUpdateShipping(
//            @PathVariable(value = "id", required = false) final Long id,
//            @NotNull @RequestBody ShippingDTO shippingDTO
//    ) throws Exception {
//        log.debug("REST request to partial update Shipping partially : {}, {}", id, shippingDTO);
//        if (shippingDTO.getId() == null) {
//            throw new Exception("Invalid id");
//        }
//        if (!Objects.equals(id, shippingDTO.getId())) {
//            throw new Exception("Invalid id");
//        }
//
//        if (!shippingRepository.existsById(id)) {
//            throw new Exception("Entity not found");
//        }
//
//        Optional<ShippingDTO> result = shippingService.partialUpdate(shippingDTO);
//
//        return ResponseEntity.of(result);
//    }

    @PostMapping(value = "/shipping/calculated/{id}")
    public String partialUpdateShipping(
            @PathVariable(value = "id", required = false) final Long id,
            @ModelAttribute ShippingDTO shipping
    ) throws Exception {
        shipping.setStatus(ShippingStatus.CALCULATED);
        shippingService.partialUpdate(shipping);
        return "redirect:/api/shipping";
    }

    @GetMapping("/shipping/calculated")
    public String getAllCalculatedShippings(Model model) {
        log.debug("REST request to get Shippings");
        List<ShippingDTO> result = shippingService
                .findAll()
                .stream()
                .filter(shippingDTO -> shippingDTO.getStatus().equals(ShippingStatus.CALCULATED))
                .sorted()
                .collect(Collectors.toList());
        model.addAttribute("calculatedShippings", result);
        return "calculatedShippings";
    }

    @GetMapping("/shipping")
    public String getAllShippings(Model model) {
        log.debug("REST request to get Shippings");
        List<ShippingDTO> result = shippingService.findAll();
        Collections.sort(result);
        model.addAttribute("shippings", result);
        return "showShippings";
    }

    @GetMapping("/shipping/calculate/{id}")
    public String calculateShipping(
            @PathVariable(value = "id", required = false) final Long id,
            Model model
    ) {
        log.debug("REST request to calculate Shipping");
        Optional<ShippingDTO> shippingDTO = shippingService.findOne(id);
        model.addAttribute("shipping", shippingDTO.orElse(new ShippingDTO()));
        return "calculateShipping";
    }

//    @GetMapping("/shipping/{id}")
//    public ResponseEntity<ShippingDTO> getShipping(@PathVariable Long id) {
//        log.debug("REST request to get Shipping : {}", id);
//        Optional<ShippingDTO> shippingDTO = shippingService.findOne(id);
//        return ResponseEntity.of(shippingDTO);
//    }

//    @DeleteMapping("/shipping/{id}")
//    public ResponseEntity<Void> deleteShipping(@PathVariable Long id) {
//        log.debug("REST request to delete Shipping : {}", id);
//        shippingService.delete(id);
//        return ResponseEntity
//                .noContent()
//                .headers(HttpHeaders.EMPTY)
//                .build();
//    }

}
