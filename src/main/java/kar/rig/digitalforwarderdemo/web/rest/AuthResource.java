package kar.rig.digitalforwarderdemo.web.rest;

import jakarta.validation.Valid;
import kar.rig.digitalforwarderdemo.domain.enumeration.UserRole;
import kar.rig.digitalforwarderdemo.service.UserService;
import kar.rig.digitalforwarderdemo.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class AuthResource {

    private final Logger log = LoggerFactory.getLogger(AuthResource.class);

    private final UserService userService;

    public AuthResource(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/")
    public String homePage() {
        return "loginPage";
    }

    @PostMapping(value = "/login")
    public String checkUser(@Valid @ModelAttribute UserDTO user) {
        Optional<UserDTO> userDTO = userService.findByMail(user.getEmail());
        if (userDTO.isPresent()) {
            if (userDTO.get().getRole().equals(UserRole.ROLE_DRIVER)) {
                return "redirect:/api/shipping";
            } else if (userDTO.get().getRole().equals(UserRole.ROLE_CUSTOMER)) {
                return "addShipping";
            }
        }
        return "redirect:/";
    }

    @GetMapping(value = "/register")
    public String registerPage() {
        return "registerPage";
    }

    @PostMapping(value = "/register")
    public String registerUser(@Valid @ModelAttribute UserDTO user) {
        userService.save(user);
        return "redirect:/";
    }
}
