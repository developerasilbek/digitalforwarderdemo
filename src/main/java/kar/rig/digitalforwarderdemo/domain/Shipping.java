package kar.rig.digitalforwarderdemo.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import kar.rig.digitalforwarderdemo.domain.enumeration.CarcaseType;
import kar.rig.digitalforwarderdemo.domain.enumeration.CurrencyType;
import kar.rig.digitalforwarderdemo.domain.enumeration.ShippingStatus;
import kar.rig.digitalforwarderdemo.domain.enumeration.TruckModel;

@Entity
@Table(name = "shipping")
public class Shipping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "sender_country", nullable = false)
    private String senderCountry;

    @NotNull
    @Column(name = "sender_address", nullable = false)
    private String senderAddress;

    @NotNull
    @Column(name = "recipient_country", nullable = false)
    private String recipientCountry;

    @NotNull
    @Column(name = "recipient_address", nullable = false)
    private String recipientAddress;

    @Enumerated(EnumType.STRING)
    @Column(name = "truck_model")
    private TruckModel truckModel;

    @Enumerated(EnumType.STRING)
    @Column(name = "carcase_type")
    private CarcaseType carcaseType;

    @NotNull
    @Column(name = "insurance", nullable = false)
    private Boolean insurance;

    @Min(value = 0, message = "Cost should not be less than 0")
    @Column(name = "total_cost")
    private Double totalCost;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private CurrencyType currency;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ShippingStatus status;

    @Column(name = "travel_time")
    private Integer travelTime;

    @ManyToOne
    private Cargo cargo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderCountry() {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry) {
        this.senderCountry = senderCountry;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getRecipientCountry() {
        return recipientCountry;
    }

    public void setRecipientCountry(String recipientCountry) {
        this.recipientCountry = recipientCountry;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public TruckModel getTruckModel() {
        return truckModel;
    }

    public void setTruckModel(TruckModel truckModel) {
        this.truckModel = truckModel;
    }

    public CarcaseType getCarcaseType() {
        return carcaseType;
    }

    public void setCarcaseType(CarcaseType carcaseType) {
        this.carcaseType = carcaseType;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public ShippingStatus getStatus() {
        return status;
    }

    public void setStatus(ShippingStatus status) {
        this.status = status;
    }

    public Integer getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(Integer travelTime) {
        this.travelTime = travelTime;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Shipping{" +
                "id=" + id +
                ", senderCountry='" + senderCountry + '\'' +
                ", senderAddress='" + senderAddress + '\'' +
                ", recipientCountry='" + recipientCountry + '\'' +
                ", recipientAddress='" + recipientAddress + '\'' +
                ", truckModel=" + truckModel +
                ", carcaseType=" + carcaseType +
                ", insurance=" + insurance +
                ", totalCost=" + totalCost +
                ", currency=" + currency +
                ", travelTime=" + travelTime +
                ", cargo=" + cargo +
                '}';
    }
}
