package kar.rig.digitalforwarderdemo.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import kar.rig.digitalforwarderdemo.domain.enumeration.TruckModel;

@Entity
@Table(name = "truck")
public class Truck {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 64)
    @Column(name = "name", length = 64, nullable = false, unique = true)
    private String name;

    @NotNull
    @Size(max = 10)
    @Column(name = "government_number", length = 10, nullable = false, unique = true)
    private String governmentNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "truck_model", nullable = false)
    private TruckModel truckModel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernmentNumber() {
        return governmentNumber;
    }

    public void setGovernmentNumber(String governmentNumber) {
        this.governmentNumber = governmentNumber;
    }

    public TruckModel getTruckModel() {
        return truckModel;
    }

    public void setTruckModel(TruckModel truckModel) {
        this.truckModel = truckModel;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", governmentNumber='" + governmentNumber + '\'' +
                ", truckModel=" + truckModel +
                '}';
    }
}
