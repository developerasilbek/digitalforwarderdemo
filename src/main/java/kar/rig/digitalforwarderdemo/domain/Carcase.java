package kar.rig.digitalforwarderdemo.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import kar.rig.digitalforwarderdemo.domain.enumeration.CarcaseType;

@Entity
@Table(name = "carcase")
public class Carcase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 64)
    @Column(name = "name", length = 64, nullable = false, unique = true)
    private String name;

    @NotNull
    @Size(max = 10)
    @Column(name = "government_number", length = 10, nullable = false, unique = true)
    private String governmentNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "carcase_type", nullable = false)
    private CarcaseType carcaseType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernmentNumber() {
        return governmentNumber;
    }

    public void setGovernmentNumber(String governmentNumber) {
        this.governmentNumber = governmentNumber;
    }

    public CarcaseType getCarcaseType() {
        return carcaseType;
    }

    public void setCarcaseType(CarcaseType carcaseType) {
        this.carcaseType = carcaseType;
    }

    @Override
    public String toString() {
        return "Carcase{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", governmentNumber='" + governmentNumber + '\'' +
                ", carcaseType=" + carcaseType +
                '}';
    }
}
