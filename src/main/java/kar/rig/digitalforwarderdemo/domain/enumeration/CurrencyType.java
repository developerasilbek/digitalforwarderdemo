package kar.rig.digitalforwarderdemo.domain.enumeration;

public enum CurrencyType {
    UZS,
    KZT,
    RUB,
    USD,
    EURO
}
