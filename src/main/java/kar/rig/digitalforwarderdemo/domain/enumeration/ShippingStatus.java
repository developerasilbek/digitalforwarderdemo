package kar.rig.digitalforwarderdemo.domain.enumeration;

public enum ShippingStatus {
    NEW,
    APPROVED,
    CALCULATED,
    CANCELLED,
    UNKNOWN
}
