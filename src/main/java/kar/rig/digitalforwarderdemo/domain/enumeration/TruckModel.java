package kar.rig.digitalforwarderdemo.domain.enumeration;

public enum TruckModel {
    MAN,
    MERCEDES_BENZ,
    SCANIA
}
