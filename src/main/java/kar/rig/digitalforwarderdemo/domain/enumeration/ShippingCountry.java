package kar.rig.digitalforwarderdemo.domain.enumeration;

public enum ShippingCountry {
    UZBEKISTAN,
    LITVA,
    LATVIA,
    RUSSIAN,
    KAZAKHSTAN
}
