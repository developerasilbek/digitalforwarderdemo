package kar.rig.digitalforwarderdemo.domain.enumeration;

public enum UserRole {
    ROLE_CUSTOMER,
    ROLE_DRIVER,
    ROLE_ADMIN
}
