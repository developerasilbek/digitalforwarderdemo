package kar.rig.digitalforwarderdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalForwarderDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalForwarderDemoApplication.class, args);
    }

}
