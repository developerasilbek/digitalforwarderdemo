package kar.rig.digitalforwarderdemo.repository;

import kar.rig.digitalforwarderdemo.domain.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Long> {
}
